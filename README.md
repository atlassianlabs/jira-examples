#Jira Lucene collectors examples
This is a set of examples for Jira 8.0 that shows how to collect Lucene search results 
in a more performant and memory efficient manner.

##Lucene in details
In Jira we have a few different type of indexes. In this example we are going to talk only about 
`issues` index.

Under the hood Lucene index is divided into segments where single segment is a set of 
documents and additional structures. Segments saved to hard drive are read-only.

A `Document` in Lucene is the equivalent of an issue in Jira. Each `document` is set of key value pairs
called `fields`. `Field` in `document` is either a system field like: issue id, project key
or a custom field from Jira.

###Collectors
In Jira you can use `SearchProvider` and get a list of `Documents`. This approach might not be always 
the most efficient one e.g. in Jira 8.0 `Documents` are stored as 
zipped files which Lucene needs to unzip in memory. 

Using collectors helps to reduce memory and CPU pressure.

In `SearchProvider.search` you can pass in a `Collector` which will collect the results of the search. 

You can collect results using:
* Documents
* DocValues

We can come here with two simple rules of thumb:
* Use collectors when you except more than 1000 issues
* Use docValues when you are collecting values from less than 20 fields

###DocValues
DocValues is a data structure in Lucene allowing you to access data of specific `document` without 
unzipping its content.

DocValues can be 1 of 6 different types that can be groupped (by collector usage) into
3 buckets:
- None - this field doesn't have its equivalent in DocValues
- Single value (Numeric, Binary, Sorted, SortedNumeric) - this field will have a single value in DocValues
- Multiple values (SortedSet) - this field can have multiple values e.g. Labels

##Examples
Currently we are providing 3 different examples:
- IssueCountPerProjectCollector - basic usage of collector
- LabelsStatisticsCollector - collector example using SortedSet DocValues
- GetDocValuesInformationExample - how to do field -> docValues type mapping

You can find more detailed information in each example.

##Performance results
We wanted to know what is the average issue count per project in Jira for 1M issues.

We tried 3 different approaches using collectors:
1. Collecting results from Documents.
2. Collecting results from Documents but with predefined fields to be loaded.
3. Collecting results from DocValues

| Approach  | Time |
| --- | --- |
| 1 | 26155ms  |
| 2 | 19774ms  |
| 3 | 853ms  |

##how to run it ?
In root directory type mvn jira:debug

Log into your Jira instance (http://localhost:2990) using admin/admin credentials.
Create a few issues and add couple labels to them.

Send REST Request: GET http://localhost:2990/jira/rest/jiraexamples/1/run_examples 

Check your console (where you ran mvn jira:debug)
 
 