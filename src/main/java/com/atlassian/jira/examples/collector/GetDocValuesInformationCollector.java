package com.atlassian.jira.examples.collector;

import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.SimpleCollector;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * This is an example showing to get information about type of doc value for given field name.
 */
public class GetDocValuesInformationCollector extends SimpleCollector {

    // Logger.
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(GetDocValuesInformationCollector.class);

    /**
     * This method is called every time the search moves to the next segment.
     * Collect with given docId is always after this method is called
     */
    @Override
    protected void doSetNextReader(LeafReaderContext context) throws IOException {
        // Segments separator
        log.info("=== Next segment ===");

        // Get fieldInfo for each field
        for (FieldInfo fieldInfo : context.reader().getFieldInfos()) {
            // Output fieldName and docValuesType
            log.info("Field name: " + fieldInfo.name + " has docValues type: " + fieldInfo.getDocValuesType());
        }
    }

    @Override
    public void collect(int doc) throws IOException {

    }

    @Override
    public boolean needsScores() {
        return false;
    }
}
