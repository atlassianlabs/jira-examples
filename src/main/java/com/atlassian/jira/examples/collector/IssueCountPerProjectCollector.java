package com.atlassian.jira.examples.collector;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.SortedDocValues;
import org.apache.lucene.search.SimpleCollector;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.issue.index.DocumentConstants.PROJECT_ID;

/**
 * This is an example showing how to access sorted doc values for a given field name
 * and count issues per project
 */
public class IssueCountPerProjectCollector extends SimpleCollector {

    // DocValues for given segment
    private SortedDocValues projKeyDocValues;

    // Final results of count per project
    private Map<String, Integer> issueCountPerProject = new HashMap<>();

    /**
     * Getter for returning issue per project count statistics
     * @return issueCount per project key
     */
    public Map<String, Integer> getIssueCountPerProject() {
        return issueCountPerProject;
    }

    /**
     * This method is called every time the search moves to the next segment.
     * Collect with given docId is always after this method is called
     */
    @Override
    protected void doSetNextReader(LeafReaderContext context) throws IOException {
        // For each segment we need to get doc values
        projKeyDocValues = context.reader().getSortedDocValues(PROJECT_ID);
    }

    /**
     * This method is called for every document that matches the query.
     * @param doc docId
     */
    @Override
    public void collect(int doc) throws IOException {
        // First we need to verify if value exists for given doc.
        if (projKeyDocValues.advanceExact(doc)) {
            // Get the value of projectKey for given doc (you need to call different methods depending on the DocValues type, e.g. .longValue() instead of .binaryValue())
            String projectKey = projKeyDocValues.binaryValue().utf8ToString();

            // Get previous value from map for given projectKey or 0 if not exist
            int count = issueCountPerProject.getOrDefault(projectKey, 0);

            // Increase counter for given project
            issueCountPerProject.put(projectKey, count + 1);
        }
    }

    @Override
    public boolean needsScores() {
        return false;
    }
}
