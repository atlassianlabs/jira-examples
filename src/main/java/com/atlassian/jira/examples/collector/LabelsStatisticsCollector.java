package com.atlassian.jira.examples.collector;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.SortedSetDocValues;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.SimpleCollector;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.issue.index.DocumentConstants.ISSUE_LABELS;
import static org.apache.lucene.index.SortedSetDocValues.NO_MORE_ORDS;


/**
 * This is an example showing how to access sorted doc values for a given field name
 * and count different labels in issues
 */
public class LabelsStatisticsCollector extends SimpleCollector {

    // DocValues for given segment
    private SortedSetDocValues labelsDocValues;

    // Final results of count per label
    private Map<String, Integer> labelsStatistics = new HashMap<>();

    /**
     * Getter for returning labels count statistics
     * @return labelsStatistics
     */
    public Map<String, Integer> getLabelsStatistics() {
        return labelsStatistics;
    }

    /**
     * This method is called every time the search moves to the next segment.
     * Collect with given docId is always after this method is called.
     */
    @Override
    protected void doSetNextReader(LeafReaderContext context) throws IOException {
        // For each segment we need to get doc values
        labelsDocValues = context.reader().getSortedSetDocValues(ISSUE_LABELS);
    }

    /**
     * This method collect if Lucene has a hit in given segment when executing query.
     * @param doc docId
     */
    @Override
    public void collect(int doc) throws IOException {
        // First we need to verify if value exists for given doc.
        if (labelsDocValues.advanceExact(doc)) {
            // Iterate over values
            long ord;
            while ((ord = labelsDocValues.nextOrd()) != NO_MORE_ORDS) {
                final BytesRef next = labelsDocValues.lookupOrd(ord);

                // Get next label value
                String label = next.utf8ToString();

                // Get previous value from map for given label or 0 if not exist
                int count = labelsStatistics.getOrDefault(label, 0);

                // Increase counter for given label
                labelsStatistics.put(label, count + 1);
            }
        }
    }

    @Override
    public boolean needsScores() {
        return false;
    }
}

