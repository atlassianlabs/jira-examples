package com.atlassian.jira.examples.impl;

import com.atlassian.jira.examples.collector.GetDocValuesInformationCollector;
import com.atlassian.jira.examples.collector.IssueCountPerProjectCollector;
import com.atlassian.jira.examples.collector.LabelsStatisticsCollector;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchQuery;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Map;

@Component
public class CollectorExamplesService {

    // Logger.
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CollectorExamplesService.class);

    // Search provider to run JQL query
    private final SearchProvider searchProvider;

    @Inject
    public CollectorExamplesService(@ComponentImport SearchProvider searchProvider) {
        this.searchProvider = searchProvider;
    }

    /**
     * Function running all examples.
     */
    public void runExamples() throws SearchException {
        runIssueCountPerProject();

        runLabelsStatistics();

        runGetDocValuesInformation();
    }


    /**
     * Doc Values information example
     */
    private void runGetDocValuesInformation() throws SearchException {
        // Collector
        GetDocValuesInformationCollector getDocValuesInformationCollector = new GetDocValuesInformationCollector();

        // Search all issues and collect them using collector above
        searchProvider.search(SearchQuery.create(JqlQueryBuilder.newBuilder().buildQuery(), null).overrideSecurity(true), getDocValuesInformationCollector);
    }

    /**
     * Labels statistics example
     */
    private void runLabelsStatistics() throws SearchException {
        // Collector
        LabelsStatisticsCollector labelsStatisticsCollector = new LabelsStatisticsCollector();

        // Search all issues and collect them using collector above
        searchProvider.search(SearchQuery.create(JqlQueryBuilder.newBuilder().buildQuery(), null).overrideSecurity(true), labelsStatisticsCollector);

        // Output results
        for (Map.Entry<String, Integer> stringIntegerEntry : labelsStatisticsCollector.getLabelsStatistics().entrySet()) {
            log.info("Label: " + stringIntegerEntry.getKey() + " is used by " + stringIntegerEntry.getValue() + " issue(s).");
        }
    }

    /**
     * Issue count per project example
     */
    private void runIssueCountPerProject() throws SearchException {
        // Collector
        IssueCountPerProjectCollector issueCountPerProjectCollector = new IssueCountPerProjectCollector();

        // Search all issues and collect them using collector above
        searchProvider.search(SearchQuery.create(JqlQueryBuilder.newBuilder().buildQuery(), null).overrideSecurity(true), issueCountPerProjectCollector);

        // Output results
        for (Map.Entry<String, Integer> stringIntegerEntry : issueCountPerProjectCollector.getIssueCountPerProject().entrySet()) {
            log.info("Project: " + stringIntegerEntry.getKey() + " has " + stringIntegerEntry.getValue() + " issue(s).");
        }
    }
}
