package com.atlassian.jira.examples.rest;

import com.atlassian.jira.examples.impl.CollectorExamplesService;
import com.atlassian.jira.issue.search.SearchException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This is a basic REST resource to help you run examples
 */
@Path("/")
@Produces({MediaType.APPLICATION_JSON})
@Named
public class CollectorExampleResource {

    // Examples service
    private final CollectorExamplesService collectorExamplesService;

    @Inject
    public CollectorExampleResource(CollectorExamplesService collectorExamplesService) {
        this.collectorExamplesService = collectorExamplesService;
    }

    /**
     * Rest endpoint for running all examples.
     */
    @Path("run_examples")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response runExamples() throws SearchException {
        collectorExamplesService.runExamples();

        return Response.ok().build();
    }


}
